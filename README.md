# Projet: figure

Auteurs: Michael Kölling and David J. Barnes
Traduction: Laurent Pierron

Ce projet fait partie du matériel pour le livre

   **Objects First with Java - A Practical Introduction using BlueJ
   Fifth edition
   David J. Barnes and Michael Kölling
   Pearson Education, 2012**

Il est expliqué dans le chapitre 1.

C'est un projet très simple pour montrer certaines caractéristiques
des objets.

Vous pouvez créer différentes figures géométriques, et vous verrez, si vous le faites,
que ces figures géométriques sont dessinées sur l'écran dans une fenêtre contenant un objet (représentant une feuille de dessin) appelé `canvas`.

Vous pouvez manipuler ces objets : changez leur position, taille et couleur.
Essayez : créez quelques carrés, triangles et cercles différents.

Ce projet est conçu comme un premier exemple de programmation orientée-objet.
Il illustre plusieurs concepts :

- un projet Java (application) est une collection de classes
- des objets peuvent être créés à partir de classes
- d'une classe quelconque peut être créé de nombreux objets
- les objets ont des opérations (méthodes)
- les opérations peuvent avoir des paramètres
- une opération peut retourner un objet
- les paramètres ont des types (au moins String et int)
- les objets possèdent des données (attributs ou champs)
- les opérations et les champs sont communs à tous les objets de la même classe
- les valeurs stockées dans les champs peuvent différées pour chaque objet

Le projet montre aussi

- la création d'objet avec BlueJ
- l'appel interactif de méthode
- le passage de paramètres

- - -

## Préparation du projet

> ATTENTION : les réponses aux questions posées font l'objet d'un quizz à compléter sur l'espace Arche du cours.

### TÂCHE 1. Créer une bifurcation (fork) de ce projet dans votre compte Bitbucket

Bifurcation de ce projet dans votre compte Bitbucket;

1. Cliquez sur le bouton **+** dans le menu à gauche, puis sur l'icône **Forker ce dépôt**, une double flèche droite, dans le menu en haut à droite de Bitbucket.
    - Une fenêtre *Fork* apparaît, vous pouvez laisser les paramètres tels quels et cliquez en bas sur **Faire un fork du dépôt**
2. Si vous travaillez en binôme, ajoutez votre binôme au projet, sur la page du projet cliquez *Envoyez une invitation*, tapez l'identifiant de votre binôme, sélectionnez les droits *Admin*.
3. Ajoutez votre enseignant au projet, sur la page du projet cliquez *Envoyez une invitation*, tapez l'identifiant de votre professeur *lcpierron* ou *laurent.pierron@inria.fr*, sélectionnez les droits *Admin*.

> Pour plus d'information sur la *bifurcation* sur Bitbucket voir la [documentation du *fork*](https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/)

### TÂCHE 2. Cloner votre projet

Le *clonage* vous permet de faire une copie locale sur votre ordinateur du dépôt (**repository** en anglais) de votre projet.

1. Cliquez sur le bouton **Cloner** dans les actions en haut à droite de la fenêtre Bitbucket.
2. Assurez vous que le protocole est bien positionné à HTTPS, si vous n'avez pas créé de clé SSH.

    Bitbucket prépare la commande de clonage pour vous.

3. Copiez la commande.
4. Ouvrez un terminal sur votre ordinateur local.
5. Naviguez vers le répertoire où vous désirez déposer vos fichiers.
6. Passez la commande à l'invite de commande.
7. Appuyez ENTER sur votre clavier.

   Git *clone* votre dépôt de Bitbucket vers votre ordinateur local.

> Si vous avez des problèmes de clonage à partir de ces instructions, vous pouvez consulter le [tutoriel plus détaillé](https://confluence.atlassian.com/bitbucket/git-tutorial-keep-track-of-your-space-station-locations-759857287.html).

- - -

## Objets et classes

Ce premier projet va vous permettre de prendre en main l'environnement de développement BlueJ en créant et manipulant graphiquement des objets.

### TÂCHE 3. Créer des objets

1. Démarrez BlueJ et ouvrir le projet **figures**, vous devriez voir le diagramme simplifié de classes UML ([*Unified Modeling Language](http://fr.wikipedia.org/wiki/UML_(informatique))) comme illustré ci-dessus.

    ![Vue de projet dans BlueJ](images/projet.png "Projet dans BlueJ") {#fig:bluej}

2. Créez un nouvel objet `Circle` en choisissant `new Circle()` dans le menu contextuel (clic sur le bouton droit) de la classe `Circle`. Choisissez comme nom : `cercle1`. L'objet apparaît dans un carré rouge en bas à gauche de la fenêtre de développement.
3. Créez un autre objet `Circle`.
4. Créez un objet `Square`.

### TÂCHE 4. Appeler des méthodes

1. Dans le menu contextuel de l'objet `cercle1`, vous voyez la liste des opérations possibles sur cet objet. Choisissez la méthode `makeVisible`. Un cercle bleu apparaît dans une fenêtre séparée.
2. Essayez les méthodes `moveRight` et `moveDown` plusieurs fois. Que se passe-t-il ?
3. Essayez la méthode `makeInvisible`. Que se passe-t-il ? Et si vous appelez la méthode `makeInvisible` deux fois de suite ?
4. Répétez ces opérations avec les deux autres objets crées. Que se passe-t-il ?

### TÂCHE 5. Passer des paramètres

1. Invoquez la méthode `moveHorizontal` sur l'objet `cercle1` après l'avoir rendu visible. Vous verrez apparaître une boîte de dialogue dans laquelle vous pouvez saisir un paramètre, entrez 50. Que se passe-t-il ?

    La boîte de dialogue contient également la **signature** de la méthode, la **signature** nous informe sur le type de la valeur retournée par la méthode, le nom de la méthode et les types et noms des `paramètres formels` de la méthode.

2. Quelle est la **signature** de la méthode `moveHorizontal` ?
3. Que signifie le mot-clé `void` dans la signature des méthodes ?
4. Essayez d'utiliser les méthodes `moveVertical`, `slowMoveVertical` et `changeSize`. Quelles sont les actions de ces méthodes ? Quelles sont leur signature ?

### TÂCHE 6. Types de données

Un **type** spécifie le genre de données que l'on peut passer en paramètres d'une méthode ainsi que le genre de la valeur retournée par une méthode si il y en a une.

1. Quels sont les **types** utilisables dans le langage de programmation Java ?
2. Appelez la méthode `changeColor` sur un objet visible et tapez le texte `"red"`. Que se passe-t-il ? Quelle est la signature de `changeColor` ?
3. Que se passe-t-il si vous tapez le texte `red` comme paramètre ? Pourquoi ?
4. Que se passe-t-il si vous saisissez une couleur non connue par le programme, comme `"rouge"` ?

### TÂCHE 7. État d'un objet

L'ensemble des valeurs de tous les attributs d'un objet (tels que la position, la couleur, la visibilité pour un objet de classe `Circle`) à un instant donné est appelé l'**état**.

1. Dans le menu contextuel de l'objet `cercle1` choisissez `Inspecter` en avant dernier dans menu.

    Une fenêtre contenant un inspecteur d'objets est alors affichée. Vous y voyez tous les attributs de l'objet et leur état courant.

    ![Inspecteur d'objets](images/inspecteur.png "Inspecteur d'objets")

1. Sans fermer cette fenêtre, invoquez la méthode `moveLeft`, que se passe-t-il dans la fenêtre d'inspection ?
1. Créez un objet des classes suivantes : `Square`, `Triangle`, `Person`, ouvrez l'inspecteur d'objets pour chacune de ces classes. Quelles sont les différences et les ressemblances ?

### TÂCHE 8. Dessiner = Programmer

Écrire un programme Java consiste à créer des objets et à appeler des méthodes sur ces objets.

1. Au moyen de l'interface graphique de BlueJ, créez le dessin suivant :

    ![Maison et soleil](images/maison.png "Maison et soleil")

    La maison est un bloc rouge, avec une fenêtre noire et un toit vert. 
    Un soleil jaune apparaît au dessus et à droite de la maison.

    Vous avez créé un premier programme !

2. Dans le menu `Voir` choisissez `Voir terminal`, une fenêtre `Terminal` s'affiche. Dans le menu de cette fenêtre choisissez `Options` et `Enregistrez les appels de méthode`. Créez un nouvel objet `Circle` de couleur verte et de diamètre 70 pixels. Que se passe-t-il dans la fenêtre `Terminal` ?

    ![Terminal](images/terminal.png "Terminal")

    Les commandes affichées dans la fenêtre `Terminal` peuvent être saisies dans la fenêtre `Code Pad` du projet.

3. Dans la fenêtre du projet, sélectionnez dans le menu `Voir` l'option `Voir Code Pad`, une zone de saisie en bas à gauche de l'écran apparaît. Entrez dans cette zone le code affiché dans le Terminal. Que se passe-t-il ? N'oubliez pas les `;` à la fin de chaque ligne.
4. En entrant des instructions dans la zone `Code Pad`, réalisez le dessin suivant :

    ![Personnages et soleil](images/personnages.png "Personnages et soleil")

    Sur une colline verte, deux personnages noirs de taille différentes se tiennent la main.
    Un petit soleil jaune brille légèrement au-dessus et à droite.

    Quel est l'intérêt d'avoir le code de création du dessin ?

- - -

### TÂCHE 9. Interaction avec des objets

Pour cette partie vous allez passer dans une sous-branche du projet figures, où la seule différence et l'ajout d'une classe `Picture`. Pour en savoir plus sur les branches Git, lisez la documentation déposée sur Arche.

1. Quittez BlueJ. Changez de branche de développement en tapant la commande `git checkout -b house origin/house` dans le fenêtre de développement, puis ouvrez le projet avec BlueJ. Quelles différence constatez-vous avec le projet précédent ?
2. Certaines classes sont grisées, il faut les compiler. Cliquez sur le bouton `compiler` à droite dans la fenêtre du projet. À quoi sert la compilation ?
3. Créez une instance de la classe `Picture` et appelez la méthode `draw`. Essayez aussi `setBlackAndWhite` et `setColor`. Que se passe-t-il ? Comment l'objet peut-il se dessiner ?

### TÂCHE 10. Modification de code source

Vous allez découvrir comment écrire dans BlueJ le code de la classe `Picture` précédente.

1. Dans le menu contextuel de la classe `Picture` sélectionnez `Éditer`, le code source de la classe apparaît dans une nouvelle fenêtre d'édition.
2. Changez le code pour afficher un soleil bleu. Que se passe-t-il dans la fenêtre du diagramme de classes ? Que faut-il faire en plus de la modification pour voir le nouveau dessin ?
3. Ajoutez un second soleil à l'image, il vous faudra ajouter un attribut avec la ligne suivante et insérer sa création et visualisation dans le code :

    ```java
    private Circle soleil2;
    ```

4. Enregistrez vos modifications en tapant la commande Git : `git commit -m "Ajout d'un soleil." Picture.java`
5. Ajoutez un coucher de soleil (*sunset*) à l'image, en utilisant la méthode `slowMoveVertical` de la classe `Circle`. Une fois que ça fonctionne, enregistrez votre travail : `git commit -m "Ajout d'un coucher de soleil." Picture.java`.
6. Placez le coucher de soleil dans une méthode `sunset` de la classe `Picture`, qu'il faudra appeler pour faire coucher le soleil. Sauvegardez votre travail : `git commit -m "Ajout d'une fonction 'sunset' de coucher de soleil." Picture.java`
7. Ajoutez une personne qui marche vers la maison après le coucher de soleil. Sauvegardez votre travail : `git commit -m "Ajout d'un personnage marchant." Picture.java`
8. Copiez vos modifications sur le serveur Bitbucket : `git push --all`

- - -
