import java.awt.*;

/**
 * Une personne qui peut �tre manipul�e et qui se dessine elle-m�me sur un canvas.
 *
 * @author Michael K�lling and David J. Barnes
 * @version 2011.07.31
 */

public class Person
{
    private int height;
    private int width;
    private int xPosition;
    private int yPosition;
    private String color;
    private boolean isVisible;

    /**
     * Cr�e une nouvelle personne � la position par d�faut avec la couleur par d�faut.
     */
    public Person()
    {
        height = 60;
        width = 30;
        xPosition = 280;
        yPosition = 190;
        color = "black";
        isVisible = false;
    }

    /**
     * Rend cette personne visible. Si elle �tait d�j� visible, ne fait rien.
     */
    public void makeVisible()
    {
        isVisible = true;
        draw();
    }
    
    /**
     * Rend cette personne invisible. Si elle �tait d�j� invisible, ne fait rien.
     */
    public void makeInvisible()
    {
        erase();
        isVisible = false;
    }
    
    /**
     * D�place la personne de quelques pixels sur la droite.
     */
    public void moveRight()
    {
        moveHorizontal(20);
    }

    /**
     * D�place la personne de quelques pixels sur la gauche.
     */
    public void moveLeft()
    {
        moveHorizontal(-20);
    }

    /**
     * D�place la personne de quelques pixels en haut.
     */
    public void moveUp()
    {
        moveVertical(-20);
    }

    /**
     * D�place la personne de quelques pixels en bas.
     */
    public void moveDown()
    {
        moveVertical(20);
    }

    /**
     * D�place la personne horizontalement de 'distance' pixels.
     * 
     * @param distance La distance de d�placement en pixels.
     */
    public void moveHorizontal(int distance)
    {
        erase();
        xPosition += distance;
        draw();
    }

    /**
     * D�place la personne verticalement de 'distance' pixels.
     *
     * @param distance La distance de d�placement en pixels.
     */
    public void moveVertical(int distance)
    {
        erase();
        yPosition += distance;
        draw();
    }

    /**
     * D�place horizontalement et lentement la personne  de 'distance' pixels.
     * 
     * @param distance La distance de d�placement en pixels.
     */
    public void slowMoveHorizontal(int distance)
    {
        int delta;

        if (distance < 0) 
        {
            delta = -1;
            distance = -distance;
        }
        else 
        {
            delta = 1;
        }

        for (int i = 0; i < distance; i++)
        {
            xPosition += delta;
            draw();
        }
    }

    /**
     * D�place verticalement et lentement la personne  de 'distance' pixels.
     * 
     * @param distance La distance de d�placement en pixels.
     */
    public void slowMoveVertical(int distance)
    {
        int delta;

        if (distance < 0) 
        {
            delta = -1;
            distance = -distance;
        }
        else 
        {
            delta = 1;
        }

        for (int i = 0; i < distance; i++)
        {
            yPosition += delta;
            draw();
        }
    }

    /**
     * Change la dimension du personnage vers la nouvelle dimension (en pixels).
     * 
     * @param newHeight La nouvelle hauteur du personnage (en pixels), doit �tre &gt;= 0.
     * @param newWidth La nouvelle largeur du personnage (en pixels), doit �tre &gt;= 0.
     */
    public void changeSize(int newHeight, int newWidth)
    {
        erase();
        height = newHeight;
        width = newWidth;
        draw();
    }

    /**
     * Change la couleur du personnage.
     * 
     * @param newColor La nouvelle couleur, les valeurs possibles sont "red", 
     *                 "yellow", "blue", "green", "magenta" and "black".
     */
    public void changeColor(String newColor)
    {
        color = newColor;
        draw();
    }

    /**
     * Dessine le personnage � l'�cran avec les caract�ristiques actuelles.
     */
    private void draw()
    {
        int bh = (int)(height * 0.7);  // hauteur du corps
        int hh = (height - bh) / 2;  // demi hauteur de la t�te
        int hw = width / 2;  // demi largeur
        int x = xPosition;
        int y = yPosition;
        if (isVisible) {
            Canvas canvas = Canvas.getCanvas();
            int[] xpoints = { x-3, x-hw, x-hw, x-(int)(hw*0.2)-1, x-(int)(hw*0.2)-1, 
                              x-hw, x-hw+(int)(hw*0.4)+1, x, x+hw-(int)(hw*0.4)-1,
                              x+hw, x+(int)(hw*0.2)+1,
                              x+(int)(hw*0.2)+1, x+hw, x+hw, x+3, x+(int)(hw*0.6), 
                              x+(int)(hw*0.6), x+3, x-3, x-(int)(hw*0.6), x-(int)(hw*0.6) };
            int[] ypoints = { y, y+(int)(bh*0.2), y+(int)(bh*0.4), y+(int)(bh*0.2), 
                              y+(int)(bh*0.5), y+bh, y+bh, y+(int)(bh*0.65), y+bh, y+bh, 
                              y+(int)(bh*0.5), y+(int)(bh*0.2), y+(int)(bh*0.4), y+(int)(bh*0.2), 
                              y, y-hh+3, y-hh-3, y-hh-hh, y-hh-hh, y-hh-3, y-hh+3 };
            canvas.draw(this, color, new Polygon(xpoints, ypoints, 21));
            canvas.wait(10);
        }
    }

    /**
     * Retire le personnage de l'�cran.
     */
    private void erase()
    {
        if (isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }
}
